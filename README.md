pg-query-parser-microservice
============================

This is very simple wrapper around amazing [pg_query library](https://github.com/pganalyze/pg_query) in a way that can be easily used over HTTP.

Setup
=====

1. Get the sources (`git clone https://gitlab.com/depesz/pg-query-parser-microservice.git`)
2. Change dir to clone (`cd pg-query-parser-microservice`)
3. Install dependencies (`bundle install`)
4. Start the service (`./control.sh start`)

Usage
=====

Once you have it started, it listens on localhost on port 15283 (unless you
configured different in the *control.sh* file).

To send query to parse you can use curl, or any other method of sending POST
request.

For example:

    curl -s -XPOST --data-urlencode "q=select 1" http://127.0.0.1:15283
    {"version":130003,"stmts":[{"stmt":{"SelectStmt":{"targetList"...

Docker
======

There are ready-made config files for docker/docker-compose.

If you're using plain docker you can build image using:

    docker build --rm -t pg-query-parser . -f docker/Dockerfile

And then you can run container with it using:

    docker run --publish 127.0.0.1:15283:15283/tcp --rm --name pg-query-parser --detach pg-query-parser

or, if you want to have it auto-started on boot:

    docker run --publish 127.0.0.1:15283:15283/tcp --restart always --name pg-query-parser --detach pg-query-parser

If you have docker-compose, you can:

    cd docker
    docker-compose up --detach

