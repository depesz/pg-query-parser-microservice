# frozen_string_literal: true

require 'roda'
require 'json'
require 'pg_query'

# Wrapper for pg_query (https://github.com/pganalyze/pg_query) that will parse
# given queries and return it as JSON over HTTP
# Query should be sent using POST to / of wherever this app is running, as
# parameter named q.
# For example:
# curl -s -XPOST --data-urlencode "q@test.sql" http://127.0.0.1:9292/
class App < Roda
  route do |r|
    r.post '' do
      response['Content-Type'] = 'application/json'

      begin
        res = PgQuery.parse(r['q']).tree.to_json
      rescue Exception => e
        res = JSON.generate({ error: e.message })
      end

      res
    end
  end
end

run App.freeze.app
