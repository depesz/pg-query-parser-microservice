#!/usr/bin/env bash

# CONFIGURATION
BIND_TO='tcp://127.0.0.1:15283'
PID_FILE='pid.file'
LOG_FILE='service.log'
SERVICE_NAME='pg-query-parser-microservice'
# CONFIGURATION

# Helper functions
show_help_page() {
    if [[ -n "${1}" ]]
    then
        exec >&2
        echo "${1}"
    fi

    cat << _END_OF_HELP_
Syntax:
  $0 (-h) command

Options:
  -h - show this help page

Commands:
  help   - shows this help page
  start  - starts ${SERVICE_NAME}
  stop   - stops ${SERVICE_NAME}
  status - shows status of ${SERVICE_NAME}. Exit code 0 means it's running, 1 means it's stopped.
_END_OF_HELP_

    [[ -n "${1}" ]] && exit 1
    exit 0
}
die() {
    echo "$@" >&2
    exit 1
}
calculate_real_paths() {
    declare -g real_pidfile
    declare -g real_logfile
    declare -g real_puma
    real_pidfile="$( readlink -f "${PID_FILE}" )"
    real_logfile="$( readlink -f "${LOG_FILE}" )"
    if ! real_puma="$( type -P puma )"
    then
        real_puma="$(
            gem contents puma |
                grep '/bin/puma$' |
                sort --reverse --version-sort |
                head -n 1
        )"
        [[ -f "${real_puma}" ]] || die "Can't find your 'puma' - did you run >bundle install<?"
    fi
}
running_pid() {
    [[ -f "${PID_FILE}" ]] || return
    local pid proc_info

    pid="$( < "${PID_FILE}" )"
    [[ "${pid}" =~ ^[1-9][0-9]*$ ]] || return

    proc_info="$( ps -p "${pid}" -nh -o cmd )" || return
    [[ "${proc_info}" == puma* ]] && echo "${pid}"
}

cmd_start() {
    local pid puma_pid
    local -a full_startup
    pid="$( running_pid )"
    [[ -n "${pid}" ]] && die "Looks that ${SERVICE_NAME} is already running, with pid ${pid}"

    mkdir -p "$( dirname "${real_pidfile}" )" "$( dirname "${real_logfile}" )" ||
        die "Couldn't make directories for pidfile or logfile"

    full_startup=( "${real_puma}" --workers 3 --preload --environment production --threads 3:10 --pidfile "${real_pidfile}" --bind "${BIND_TO}" )

    echo "Starting ${SERVICE_NAME} with: ${full_startup[*]}" >> "${real_logfile}" || die "Can't write to ${real_logfile}"
    echo '0' > "${real_pidfile}" || die "Can't write to ${real_pidfile}"

    nohup "${full_startup[@]}" < /dev/null &> "${real_logfile}" &
    puma_pid="$!"

    echo "${SERVICE_NAME} started with PID: ${puma_pid}, with logs in: ${real_logfile}"
    exit 0
}

cmd_stop() {
    local pid
    pid="$( running_pid )"
    if [[ -z "${pid}" ]]
    then
        echo "${SERVICE_NAME} is already stopped."
        exit 0
    fi
    echo -ne "Stopping ..."
    kill "${pid}"
    for _ in {1..5}
    do
        sleep .1
        pid="$( running_pid )"
        if [[ -z "${pid}" ]]
        then
            echo " stopped."
            exit 0
        fi
    done

    echo -n " trying harder ..."
    kill -9 "${pid}"
    for _ in {1..5}
    do
        sleep .1
        pid="$( running_pid )"
        if [[ -z "${pid}" ]]
        then
            echo " stopped."
            exit 0
        fi
    done

    echo "can't stop it?!"
    exit 1
}

cmd_status() {
    local pid
    pid="$( running_pid )"
    if [[ -z "${pid}" ]]
    then
        echo "${SERVICE_NAME} is stopped."
        exit 1
    fi
    echo "${SERVICE_NAME} is running with pid: ${pid}."
    printf "%-14s: %s\n" 'Log' "${real_logfile}"
    printf "%-14s: %s\n" 'PID-file' "${real_pidfile}"
    printf "%-14s: %s\n" 'puma binary' "${real_puma}"
    printf "%-14s: %s\n" 'Listening on' "${BIND_TO}"
    exit 0
}
# Helper functions

# Main script

# Make sure that current working directory is where the script is
cd "$( dirname "${BASH_SOURCE[0]}" )" || die "Can't chdir to ${BASH_SOURCE[0]}, can't proceed."

# Check options

while getopts ':vh' optname
do
    case "${optname}" in
        h)   show_help_page                             ;;
        v)   echo verbose ;;
        *)   show_help_page "Unknown option: ${OPTARG}" ;;
    esac
done
shift "$(( OPTIND - 1 ))"

(( $# == 0 )) && show_help_page 'You have to give a command'
(( $# > 1 )) && show_help_page 'You can only give one command'

calculate_real_paths

cmd="${1}"

case "${cmd}" in
    start)    cmd_start                                 ;;
    stop)     cmd_stop                                  ;;
    status)   cmd_status                                ;;
    help)     show_help_page                            ;;
    *)        show_help_page "Unknown command: ${cmd}"  ;;
esac

exit 0
